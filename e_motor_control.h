/* 
 * File:   e_motor_control.h
 * Author: Edney
 *
 * Created on 31 de Julho de 2015, 19:02
 */

#ifndef E_MOTOR_CONTROL_H
#define	E_MOTOR_CONTROL_H

#include "e_motors.h"
#include "e_epuck_ports.h"
#include "e_init_port.h"
#include <math.h>
#include "bluetooth/e_vulnerability.h"
#define PI 3.14159 //Pi
#define R 36.8 // e-puck�s radius
#define N 77.64 // motor steps to perform 1cm
#define D 3.67 // motor steps to perform 1� rotation

/**
 * Move the robot in centimeter
 * @param distance: amount centimeters to move
 */
void moveCm(int distance); 
/**
 * Rotate the robot in degree
 * @param angle
 */
void rotationDegree(int angle); 
/**
 * Move the robot from one point to another
 * @param x: initial x coordinate
 * @param y: initial y coordinate
 * @param angle: initial angle in degree
 * @param targetx: final x coordinate
 * @param targety: final y coordinate
 */
void moveToPosition(float x,float y, float angle, float targetx, float targety); 
/**
 * 
 * @param lspeed
 * @param rspeed
 * @param stepcount
 */
void drive(int lspeed, int rspeed, int stepcount);
void moveToBestPosition(VulnerabilityData *data, int twoHopSize);
void makeRequest(void);
void leaveRequest(void);
int isInterrupted();
void resetInterrupt();
int isBusy();
void resetBusy();

#endif	/* E_MOTOR_CONTROL_H */