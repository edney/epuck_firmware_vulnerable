#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "e_bluetooth.h"
#include "e_uart_char.h"
#include "e_epuck_ports.h"
#include "e_init_port.h"
#include "e_led.h"
#include "e_ad_conv.h"
#include "e_prox.h"
#include "e_motors.h"
#include "e_motor_control.h"
#include "bluetooth/e_bluetooth_handler.h"
#include "utility/utility.h"
#include "bluetooth/e_vulnerability.h"

#define BUFFER_SIZE 120
#define THRESHOLD 0.5 
#define ID 2948
#define PROX_RANGE 300
#define PROX_FACTOR 50

char msgFree[4] = "FREE";
char msgVulnerability[10];
char msgThreshold[5];

char buffer[BUFFER_SIZE]={'\0'};
float parameters[5]={'\0'};
char id[7];
char checkVuln[7];

VulnerabilityData vulnerabilityData;
float vulnerability;
char strvulnerabilidade[10]={'\0'};
char global[10]={'\0'};
char xvalue[10]={'\0'};
char yvalue[10]={'\0'};

int disableTMR1;
int enableTMR1;
int isVulnerable;
int blink;
int obstacle;
int busyMove;
int calibrated[8];
int countCycle;
int countUndecided;
int flagUndecided;
int flagInterrupted;
float threshold[1];
int X;  //flaf for check vulnerability

void avoidColision(void);

// timer 1 interrupt
// this code is executed every UPDATE_TIMING ms
void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt(void) {
    IFS0bits.T1IF = 0;                      // clear interrupt flag
    
    avoidColision();
}

/* init the Timer 1 */
void InitTMR1(void) {
    T1CON = 0;
    T1CONbits.TCKPS = 3;                    // prescsaler = 256
    TMR1 = 0;                               // clear timer 1
    PR1 = (150*MILLISEC)/256.0;   // interrupt after UPDATE_TIMING ms
    IFS0bits.T1IF = 0;                      // clear interrupt flag
    IEC0bits.T1IE = 1;                      // set interrupt enable bit
    T1CONbits.TON = 1;                      // start Timer1
}

/* disable the Timer 1 */
void DisableTMR1(void) {
    IFS0bits.T1IF = 0;                      // clear interrupt flag
    IEC0bits.T1IE = 0;                      // set interrupt disable bit
}

void calibrateIR() {
    int i, j;
    long sensor[8];

    for (i = 0; i < 8; i++) {
        sensor[i] = 0;
        calibrated[i] = 0;
    }

    for (j = 0; j < 32; j++) {
        for (i = 0; i < 8; i++)
            sensor[i] += e_get_prox(i);
        wait(10000);
    }

    for (i = 0; i < 8; i++) {
        calibrated[i] = sensor[i] / 32;
    }
}

void avoidColision(){
        
    blink = !blink;
    //e_set_led(4,blink);
    
    if(isVulnerable){
             
        if (e_get_prox(0) - calibrated[0] > PROX_RANGE || e_get_prox(1) - calibrated[1] > PROX_RANGE || e_get_prox(2) - calibrated[2] > PROX_RANGE + PROX_FACTOR) {
            obstacle = 1;
            busyMove = 1;
            makeRequest();
            e_set_speed_left(-300);
            e_set_speed_right(300);
            countCycle = 0; 
            
            if(!flagUndecided){
                countUndecided++;
                flagUndecided = 1;
            }
        }

        else if (e_get_prox(5) - calibrated[5] > PROX_RANGE + PROX_FACTOR || e_get_prox(6) - calibrated[6] > PROX_RANGE || e_get_prox(7) - calibrated[7] > PROX_RANGE ) {
            obstacle = 1; 
            busyMove = 1;
            makeRequest();
            e_set_speed_left(300);
            e_set_speed_right(-300);
            countCycle = 0; 
            
            if(flagUndecided){
                countUndecided++;
                flagUndecided = 0;
            }
        }

        else {
            if(obstacle){
                makeRequest();
                busyMove = 1;
                e_set_speed_left(300);
                e_set_speed_right(300);
                countCycle++; 
            }
            
            if(countCycle > 17){
                obstacle = 0;
                isVulnerable = 0;
                flagInterrupted = 0;
                resetInterrupt();
                countCycle = 0;
                busyMove = 0;
                resetBusy();
                e_set_speed_left(0);
                e_set_speed_right(0);
                leaveRequest();
                sendBluetoothString(msgFree, strlen(msgFree));
            }
            
            if(countUndecided > 10){
                obstacle = 0;
                isVulnerable = 0;
                flagInterrupted = 0;
                resetInterrupt();
                countCycle = 0;
                busyMove = 0;
                resetBusy();
                e_set_speed_left(0);
                e_set_speed_right(0);
                leaveRequest();
                sendBluetoothString(msgFree, strlen(msgFree));
                countUndecided = 0;
            }
        }
    }
}

int main(void) {

    e_init_port();	
    e_init_uart1();
	e_init_ad_scan(ALL_ADC);
    
    isVulnerable = 0; 
    obstacle = 0;
    busyMove = 0;
    disableTMR1 = 1;
    enableTMR1 = 0;
    blink = 0;
    countCycle = 0;
    flagInterrupted = 0;
    flagUndecided = 0;
    countUndecided = 0;
    threshold[0] = 0.5;
    
    if(RCONbits.POR){
	RCONbits.POR=0;
	__asm__ volatile ("reset");
    }
    
    InitTMR1();
    calibrateIR();
   
    if(getselector() == 2) {

        while (1){
            
            e_set_led(6,isVulnerable);
            
            if(e_ischar_uart1()){
                
                memset(&buffer[0], 0, sizeof(buffer));
                receiveBluetoothString(buffer);
                
                wait(20);
               
                switch(buffer[0]){
                    case 'M':
                        
                        if(disableTMR1){
                            DisableTMR1();
                            disableTMR1 = 0;
                        }
                        sendBluetoothString(buffer, strlen(buffer));
                        parseMessageToMove(buffer,parameters);
                        moveToPosition(parameters[0],parameters[1],parameters[2],parameters[3],parameters[4]);
                       
                        break;
                     
                    case 'I':
                                                
                        sprintf(id,"ID-%d",ID);
                        sendBluetoothString(id, strlen(id));
                        break;
                    
                    case 'T':
                        parseMessageToThreshold(buffer,threshold);                        
                        ftoa(threshold[0],msgThreshold,3); 
                        sendBluetoothString(msgThreshold, strlen(msgThreshold));
                        break;     
                        
                    case 'X':
                        
                        sprintf(checkVuln,"XX-%d",X);
                        sendBluetoothString(checkVuln, strlen(checkVuln));
                        break;     
                    
                    case 'R':
                        
                        memset(&global[0], 0, sizeof(global));
                        ftoa(vulnerabilityData.vulnerabilityGlobal,global,1); 
                        sendBluetoothString(global, strlen(global));
                        
                        memset(&xvalue[0], 0, sizeof(xvalue));
                        ftoa(vulnerabilityData.x,xvalue,1); 
                        sendBluetoothString(xvalue, strlen(xvalue));
                        
                        memset(&yvalue[0], 0, sizeof(yvalue));
                                              
                        ftoa(vulnerabilityData.y,yvalue,1); 
                        sendBluetoothString(yvalue, strlen(yvalue));
                        break;    
                        
                    case 'V':
                        
                        parseMessageToVulnerability(buffer, &vulnerabilityData);
                        memset(&buffer[0], 0, sizeof(buffer));
                        int i=0;
                        do{
                            if(e_ischar_uart1()){
                               receiveBluetoothString(buffer); 
                               if(buffer[0] != 'f'){
                                   parseMessageToNeighbors(buffer, &vulnerabilityData, i);
                                   i++;
                               }
                            }
                        }while(buffer[0] != 'f' );
                        
                        memset(&buffer[0], 0, sizeof(buffer));
                        
                        int j = 0;
                                
                        do{
                            if(e_ischar_uart1()){
                               receiveBluetoothString(buffer); 
                               if(buffer[0] != 'f'){
                                   parseMessageToTwoHopNeighbors(buffer, &vulnerabilityData, j);
                                   j++;
                               }
                            }
                        }while(buffer[0] != 'f' );

                        vulnerability =  evaluateVulnerability(&vulnerabilityData, vulnerabilityData.countNeighbors, vulnerabilityData.countTwoHops );
                        memset(&strvulnerabilidade[0], 0, sizeof(strvulnerabilidade));
                        ftoa(vulnerability,strvulnerabilidade,3); 
                        
                        
                        float randValue = (float)rand()/(float)(RAND_MAX/1);
                        
                        if(vulnerability > randValue){
                            
                            isVulnerable = 1;
                            X = 1111;
                            sprintf(msgVulnerability,"VULS:%s",strvulnerabilidade);
                            sendBluetoothString(msgVulnerability, strlen(msgVulnerability));
                        
                        }else{
                            isVulnerable = 0;
                            X = 0000;
                            sprintf(msgVulnerability,"VULN:%s",strvulnerabilidade);
                            sendBluetoothString(msgVulnerability, strlen(msgVulnerability));
                        }
                        break;    
                       
                    default: // silently ignored
                        break;
                }
            }
            
            busyMove = isBusy();
            
            if((isVulnerable) && (obstacle == 0) && (!busyMove)){ //FAZER A LOGICA DO BUSY E A LOGICA PARA ZERAR OS VULNERABILITY DATA TODA VEZ QUE RECEBER OS DADOS DO COMPUT
                
                moveToBestPosition(&vulnerabilityData, vulnerabilityData.countTwoHops);
                
                flagInterrupted = isInterrupted();
                
                if(!flagInterrupted){
                    isVulnerable = 0;
                    flagInterrupted = 0;
                    resetInterrupt();
                }
            }else if(obstacle){
            
            }else{
                NOP();
            }
        }
    }else{
        NOP();
    }
  return 1;
}