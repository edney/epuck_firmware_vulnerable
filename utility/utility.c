#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../utility/utility.h"
#include <assert.h>
#include <math.h>
#include "../e_epuck_ports.h"
#include "../e_init_port.h"
#include "../bluetooth/e_bluetooth_handler.h"
#include "../bluetooth/e_vulnerability.h"

int minValue(int* array, int size){
    int val = array[0];
    int i;
    for (i = 1; i < size; ++i){
        val = val <= array[i] ? val : array[i];
    }
    return val;
}

void wait(long num) {
	long i;
	for(i=0;i<num;i++);
}

int getselector() {
	return SELECTOR0 + 2*SELECTOR1 + 4*SELECTOR2 + 8*SELECTOR3;
}

void parseMessageToThreshold(char* buffer, float* threshold){
    int i;
    char value[10]; 
    
    for (i = 2; i < strlen(buffer); i++) {
        value[i-2] = buffer[i];
    }
    sscanf(value,"%f",&threshold[0]);
    
}

void parseMessageToMove(char* buffer, float* converted){
    
    char temp;
    int i;
    int j;
    int k;
    j = 0;
    k = 0;
    
    char value[10]; 
    
    for (i = 2; i < strlen(buffer); i++) {
        
        temp = buffer[i];
        if(temp != ','){
            value[k] = temp;
            k++;
        }else{
            sscanf(value,"%f",&converted[j]);
            j++;
            k = 0;
        }
    }
    sscanf(value,"%f",&converted[j]);
}

void parseMessageToVulnerability(char *buffer,VulnerabilityData *data){
    
    float converted[5];
    char temp;
    int i;
    int j;
    int k;
    j = 0;
    k = 0;
    char value[10]; 
    
    for (i = 2; i < strlen(buffer); i++) {
        
        temp = buffer[i];
        if(temp != ','){
            value[k] = temp;
            k++;
        }else{
            sscanf(value,"%f",&converted[j]);
            j++;
            k = 0;
        }
    }
    sscanf(value,"%f",&converted[j]);
    
    data->vulnerabilityGlobal = converted[0];
    data->x = converted[1];
    data->y = converted[2];
    data->angle = converted[3];
    data->countNeighbors = 0;
    data->countTwoHops = 0;
}

void parseMessageToNeighbors(char *buffer,VulnerabilityData *data, int index){
    
    float converted[5];
    char temp;
    int i;
    int j;
    int k;
    j = 0;
    k = 0;
    
    char value[10]; 
    
    for (i = 2; i < strlen(buffer); i++) {
        
        temp = buffer[i];
        if(temp != ','){
            value[k] = temp;
            k++;
        }else{
            sscanf(value,"%f",&converted[j]);
            j++;
            k = 0;
        }
    }
    sscanf(value,"%f",&converted[j]);
    
    Neighbor tempNeighbor;
    tempNeighbor.id = converted[0];
    tempNeighbor.idParent = converted[1];
    tempNeighbor.x = converted[2];    
    tempNeighbor.y = converted[3];
    tempNeighbor.vulnerability = converted[4];        
      
    data->neighbor[index] = tempNeighbor; 
    data->countNeighbors = data->countNeighbors + 1;
}

void parseMessageToTwoHopNeighbors(char *buffer,VulnerabilityData *data, int index){
    float converted[5];
    char temp;
    int i;
    int j;
    int k;
    j = 0;
    k = 0;
    
    char value[10]; 
    
    for (i = 2; i < strlen(buffer); i++) {
        
        temp = buffer[i];
        if(temp != ','){
            value[k] = temp;
            k++;
        }else{
            sscanf(value,"%f",&converted[j]);
            j++;
            k = 0;
        }
    }
    sscanf(value,"%f",&converted[j]);
    
    Neighbor tempNeighbor;
    tempNeighbor.id = converted[0];
    tempNeighbor.idParent = converted[1];
    tempNeighbor.x = converted[2];    
    tempNeighbor.y = converted[3];
    tempNeighbor.vulnerability = converted[4];        
      
    data->twohop[index] = tempNeighbor; 
    data->countTwoHops = data->countTwoHops + 1;
}

// reverses a string 'str' of length 'len'
void reverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}
 
 // Converts a given integer x to string str[].  d is the number
 // of digits required in output. If d is more than the number
 // of digits in x, then 0s are added at the beginning.
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x)
    {
        str[i++] = (x%10) + '0';
        x = x/10;
    }
    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';
 
    reverse(str, i);
    str[i] = '\0';
    return i;
}
 
// Converts a floating point number to string.
void ftoa(float n, char *res, int afterpoint)
{
    // Extract integer part
    int ipart = (int)n;
    // Extract floating part
    float fpart = n - (float)ipart;
    // convert integer part to string
    int i = intToStr(ipart, res, 0);
 
    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.';  // add dot
 
        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);
 
        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}