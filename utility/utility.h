#include "../bluetooth/e_vulnerability.h"

int minValue(int* array, int size);
void wait(long num);
int getselector();
void parseMessageToMove(char* buffer, float* converted);
void parseMessageToThreshold(char* buffer, float* threshold);
void parseMessageToVulnerability(char *buffer,VulnerabilityData *data);
void parseMessageToNeighbors(char *buffer,VulnerabilityData *data, int i);
void parseMessageToTwoHopNeighbors(char *buffer,VulnerabilityData *data, int index);
void ftoa(float n, char *res, int afterpoint);
int count(int* array);
void removeItem(int *array, int item);