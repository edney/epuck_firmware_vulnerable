#include "e_motor_control.h"
#include "e_led.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include "bluetooth/e_vulnerability.h"
#include "utility/utility.h"
#include "bluetooth/e_bluetooth_handler.h"

#define LONG_MAX (sizeof(long) << 1) - 1
#define LONG_MIN -(sizeof(long) << 1)
#define INT_MAX (sizeof(int) << 1) - 1
#define INT_MIN -(sizeof(int) << 1)

#define DIAMETER 41.68
#define CIRCUMFERENCE (DIAMETER * PI)
#define MOTOR_STEPS_PER_REVOLUTION 20
#define MOTOR_TO_WHEEL_GEAR_RATIO 50
#define STEPS_PER_REVOLUTION (MOTOR_STEPS_PER_REVOLUTION * MOTOR_TO_WHEEL_GEAR_RATIO)
#define STEP_PER_D (float)(STEPS_PER_REVOLUTION / CIRCUMFERENCE)
#define MM_TO_STEPS(mm) (int)(mm * STEP_PER_D)

#define WHEEL_SEPARATION 54.33
#define STEPS_FOR_FULL_CIRCLE (int)((WHEEL_SEPARATION / DIAMETER) * STEPS_PER_REVOLUTION)

#define ACCON

int request = 0;
int interrupted = 0;
int busy = 0;

void moveCm(int distance) // distance in cm
{
    int steps;
    float temp = 0;
    int i, neg = 0;
    if (distance < 0) {
        neg++;
        distance = -distance;
    }
    for (i = 0; i < distance; i++) {
        temp += N;
    }
    steps = (int) temp;
    if (neg == 1){
        drive(-600,-600,steps);
        
    }else{
        drive(600,600,steps);
    }
}

void makeRequest(){
    request = 1;
}

void leaveRequest(){
    request = 0;
}

int isInterrupted(){
    return interrupted;
}

void resetInterrupt(){
    interrupted = 0;
}

int isBusy(){
    return busy;
}

void resetBusy(){
    busy = 0;
}

void rotationDegree(int angle) { // angle in degrees:positive angle - clockwise; negative angle anti-clockwise
    
    if(angle > 180){  // get the complement angle for minimal rotation robot
        
        int complement = angle - 180;
        angle = -(180 - complement);
    }
    int steps, i;
    float tmp = 0;
    int neg = 0;
    if (angle < 0) {
        neg++;
        angle = -angle;
    }
    for (i = 0; i < angle; i++) {
        tmp += D; 
    }
    steps = (int) tmp;
    if (neg == 1){
        drive(-600,600,steps);
    }else{
        drive(600,-600,steps);
    }   
}

void moveToPosition(float x,float y, float angle, float targetx, float targety){ //to move from one position to a target

    float rotate;
    
    if(((targetx - x) != 0) && ((targety -y) != 0)){
        
        if(targetx < x){
            
            if(angle > 180.0){
                rotate = angle - 180.0 + ((180.0/PI)*(atanf((targety-y)/(targetx-x))));
            }else{
                rotate = angle + 180.0 + ((180.0/PI)*(atanf((targety-y)/(targetx-x))));
            }
            rotationDegree(rotate);
        }else{
        
            rotate = angle + ((180.0/PI)*(atanf((targety-y)/(targetx-x))));
            rotationDegree(rotate);
        }
    }
    else{
        if(targety > y){
            rotationDegree(angle + 90);
        }
        else if(targety == y){
            if(targetx >= x){
                rotationDegree(angle);
            }
            else{
                if((angle + 180) > 360){
                    rotationDegree(angle - 180);
                }
                else{
                    rotationDegree(angle + 180);
                }
            }
        }
        else {
            rotationDegree(angle - 90);
        }
    }

    int distance = sqrtf(powf(targety - y,2) + powf(targetx - x,2));
    moveCm(distance);
}

int abs(int x) {if(x < 0)       return -x;      return x;}
int max(int x, int y) { return (x>y) ? x : y;   }

void drive(int lspeed, int rspeed, int stepcount) {
        
    
        int mode, count, l1, r1, l2 = 0, r2 = 0, lastcount = 0;
        float f, g;
        int lm = (lspeed < 0 ? -1 : 1);
        int rm = (rspeed < 0 ? -1 : 1);

        lspeed = abs(lspeed);
        rspeed = abs(rspeed);

        e_set_steps_left(0);
        e_set_steps_right(0);

        if(lspeed > rspeed) {
                mode = 0;
        } else {
                mode = 1;
        }
        
#ifdef ACCON
        l1 = 100;
        r1 = 100;
#else
        l1 = lspeed;
        r1 = rspeed;
#endif
        while(mode >= 0) {
            busy = 1;
            
            if(request){
                interrupted = 1;
                break;
            }
            if(mode == 0) count = e_get_steps_left();
            else    count = e_get_steps_right();
                count = abs(count);
                if(l1 != l2) {
                        e_set_speed_left(l1 * lm);
                        lastcount = count;
                        l2 = l1;
                }
                if(r1 != r2) {
                        e_set_speed_right(r1 * rm);
                        lastcount = count;
                        r2 = r1;
                }
                if(count >= stepcount) {
                        mode = -1;
                }
#ifdef ACCON
                if(count - lastcount > 5) {
                        f = 2 * (float)count / stepcount;
        
                        if(f > 1)       f = 2 - f;

                        g = 1-f;
                        f = 1 - (g*g*g*g*g*g*g*g);
        
                        l1 = (int)(lspeed * f);
                        r1 = (int)(rspeed * f);
                }
#endif
        }
        e_set_speed_left(0);
        e_set_speed_right(0);
        busy = 0;
}

void moveToBestPosition(VulnerabilityData *data, int twoHopSize){
    
    int i;
    int countPath = 0;
    int pathB = 0;
    int listPathBID[twoHopSize];
    int listPathBX[twoHopSize];
    int listPathBY[twoHopSize];
       
    for (i = 0; i < twoHopSize; i++) {
        listPathBID[i] = 0;
        listPathBX[i] = 0;
        listPathBY[i] = 0;
    }

    for (i = 0; i < twoHopSize; i++) {
        int currentID;
        int currentX;
        int currentY;
        currentID = data->twohop[i].id;
        currentX = data->twohop[i].x;
        currentY = data->twohop[i].y;
                        
        int j;
        for (j = 0; j < twoHopSize; j++) {
            if (currentID == data->twohop[j].id) {
                countPath++;
            }
        }
        
        if(countPath < 2){
            listPathBID[pathB] = currentID;
            listPathBX[pathB] = currentX;
            listPathBY[pathB] = currentY;
            pathB++;
        }
        countPath = 0;
    }
        
    float bestXPosition = 0.0;
    float bestYPosition = 0.0;
    
    for (i = 0; i < pathB; i++) {

        bestXPosition = bestXPosition + listPathBX[i];
        bestYPosition = bestYPosition + listPathBY[i];
    }
    
    float Xgoal = bestXPosition/(float)pathB;
    float Ygoal = bestYPosition/(float)pathB;
     
    moveToPosition(data->x,data->y,data->angle,Xgoal,Ygoal);
    
    //char xgoal[8]={'\0'};
    //char ygoal[8]={'\0'};
    //char msgGOAL[20];
          
    //ftoa(Xgoal,xgoal,3);
    //ftoa(Ygoal,ygoal,3); 
    //sprintf(msgGOAL,"X:%sY:%s",xgoal,ygoal);
    //sendBluetoothString(msgGOAL, strlen(msgGOAL));
}