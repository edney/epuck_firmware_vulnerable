#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "e_bluetooth.h"
#include "../e_uart_char.h"
#include "../e_epuck_ports.h"
#include "../e_init_port.h"
#include "../e_led.h"
#include "../e_prox.h"
#include "../e_motors.h"
#include "../utility/utility.h"
#include "e_bluetooth_handler.h"

void sendBluetoothString(char *data, char datalength){
    
    char send[120];
    char enviar[120];
    int i;
    
    send[0] = 0x16;
    send[1] = 0x02;
    
    for (i=0;i<datalength;i++)
		send[i+2]=data[i];
    
    send[2+datalength]=0x03;
    send[3+datalength]=0x17;
    
    sprintf(enviar,"%s",send);
    
    e_send_uart1_char(enviar,strlen(enviar));
    
    while(e_uart1_sending())
         NOP();
    
    memset(&enviar[0], 0, sizeof(enviar));
}
void receiveBluetoothString(char *buffer){
     
    char bt_buffer_in[120];
    
    while (1){
        int count = 0;
        while(!e_ischar_uart1())
            NOP();
        e_getchar_uart1(&bt_buffer_in[count]);

        if(bt_buffer_in[count] != 0x16)
            continue;
        count = 1;

        while(!e_ischar_uart1())
            NOP();
        e_getchar_uart1(&bt_buffer_in[count]);

        if(bt_buffer_in[count] != 0x02)
            continue;
        do {
            while(!e_ischar_uart1())
                NOP();
            count++;
            e_getchar_uart1(&bt_buffer_in[count]);
        } while (bt_buffer_in[count] != 0x03);

        while(!e_ischar_uart1())
            NOP();
        e_getchar_uart1(&bt_buffer_in[count+1]);

        if(bt_buffer_in[count+1] != 0x17)
            continue;
        int j;
        for(j=0; j < count; j++){
            buffer[j] = bt_buffer_in[j+2];
        }
        break;
    }
}